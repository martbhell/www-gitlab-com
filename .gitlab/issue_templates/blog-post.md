<!-- PLEASE READ: See https://about.gitlab.com/handbook/marketing/blog/ for details on the blog process. Unless you are just suggesting a blog post idea, you will need to create the formatted merge request for your blog post in order for it to be reviewed and published. -->

### Proposal

<!-- What do you want to blog about? Add your description here. If you are making an announcement, please open an `announcement` request issue in the Corporate Marketing project instead: https://about.gitlab.com/handbook/marketing/corporate-marketing/#requests-for-announcements -->

### Checklist

- [ ] If you have a specific publish date in mind (please allow 3 weeks' lead time)
  - [ ] Include it in the issue title and apply the appropriate milestone (e.g. `Blogs October 2020`)
  - [ ] Give the issue a due date of a minimum of 2 working days prior
  - [ ] If your post is likely to be >2,000 words, give a due date of a minimum of 4 working days prior
- [ ] If [time sensitive](https://about.gitlab.com/handbook/marketing/blog/#process-for-time-sensitive-posts)
  - [ ] Add ~"Blog: Priority" label and supplied rationale in description
  - [ ] Mention `@rebecca` to give her a heads up ASAP
- [ ] If wide-spread customer impacting or sensitive, mention `@nwoods` to give her a heads up ASAP, apply the ~"sensitive" label, and check the [PR handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/#requests-for-announcements) in case you need to open an announcement request instead of a blog post issue
- [ ] If the post is about one of GitLab's Technology Partners, including integration partners, mention `@TinaS`,  apply the ~"Partner Marketing" label, and see the [blog handbook for more on third-party posts](https://about.gitlab.com/handbook/marketing/blog/index.html#third-party-posts)
- [ ] If the post is about one of GitLab's customers, mention `@KimLock` and `@FionaOKeeffe`, apply the ~"Customer Reference Program" label, and see the [blog handbook for more on third-party posts](https://about.gitlab.com/handbook/marketing/blog/index.html#third-party-posts)
- [ ] Indicate if supporting an event or campaign
- [ ] Indicate if this post requires additional approval from internal or external parties before publishing (please provide details in a comment)

/label ~"blog post" ~"Blog::Pitch"
