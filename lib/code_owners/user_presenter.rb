module Gitlab
  module CodeOwners
    class UserPresenter
      PATH_TO_IMAGES = '/images/team/'.freeze
      PLACEHOLDER_IMAGE = 'gitlab-logo-extra-whitespace.png'.freeze

      def initialize(team)
        @team = team
      end

      def present(username)
        return render(username) unless gitlab_name?(username)

        gitlab_name = username.gsub(/@/, '')
        user = team.find { |user| user['gitlab'] == gitlab_name } || { 'name' => username }

        render(user['name'], url: "https://gitlab.com/#{gitlab_name}", gitlab_name: username, image_url: image_url(user['picture']))
      end

      private

      attr_reader :team

      def render(description, url: nil, gitlab_name: nil, image_url: nil)
        {
          description: description,
          image_url: image_url,
          url: url,
          gitlab_name: gitlab_name
        }
      end

      def gitlab_name?(username)
        username.start_with?('@')
      end

      def image_url(picture_name)
        return unless picture_name

        ::File.join(PATH_TO_IMAGES, detect_picture_file(picture_name))
      end

      def detect_picture_file(picture_name)
        return picture_name if picture_name.include?(PLACEHOLDER_IMAGE)

        ::File.basename(picture_name, ::File.extname(picture_name)) + "-crop.jpg"
      end
    end
  end
end
