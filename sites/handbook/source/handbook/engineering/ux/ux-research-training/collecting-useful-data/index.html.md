---
layout: handbook-page-toc
title: "Collecting Useful Data"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


### Decide what kinds of insights to collect
As you take notes, it’s easy to think that everything you hear and/or see is important, but this makes it extremely difficult to analyze the information across all your participants. You can use your research objectives to create categories you can reference as you take notes to make sure you are capturing the most salient information. Focus on the need-to-know information, not the nice-to-know information.

### Decide on a collection method
Conducting user research often results in a pretty hefty amount of information. To stay organized throughout this process, spend some time upfront deciding how you’ll collect your notes. Using Dovetail will help consolidate your data as well as facilitates highlighting and tagging content. Doing this makes it easier for your team to collaborate by adding their own notes and observations, and collecting everything in a central place will keep things organized and consistent. 

Here are some note-taking resources to use, in addition to Dovetail, depending on the type of study you're conducting:

* [A guide to user research note taking](https://condens.io/user-research-note-taking/) This guide shows how to take good notes to save time later on. It also compares alternatives to note taking in a spreadsheet -- for example, using sticky notes.
* [User Interview Note Taking Template](https://docs.google.com/spreadsheets/d/1hnIqg-fnCYW2XKHR8RBsO3cYLSMEZy2xUKmbiUluAY0/edit#gid=0)
* [Usability Testing Rainbow Analysis Chart](https://docs.google.com/spreadsheets/d/1bPg6op9Sk46lFVGaET-fruE0qz-ctNQsxbZKF-5lpn4/edit#gid=0). This approach uses a templated and color-coded spreadsheet to record what participants did during the test. For a thorough walkthrough on how to use this method, check out [this article](https://userresearch.blog.gov.uk/2019/09/13/how-a-spreadsheet-can-make-usability-analysis-faster-and-easier/), or watch [this video of a GitLab researcher's experience using this method](https://drive.google.com/file/d/1fYRTmaHZjMwDQfAnVpaEqHP1dByy1X5x/view?usp=sharing) (starts at 7:00).

### Identify additional notetakers
A note-taking best practice is to have multiple note takers for each research session. Having more than one note-taker helps by:

* **Gaining insights and developing high-quality solutions**, as observers interpret incidents through their diverse professional points of view.
* **Engaging the team constructively**, as everyone understands usability problems together.
* **Generating top findings together to get consensus quickly** and motivate the team to fix problems.
* **Multiple people often capture different insights**

Scheduling your session on the UX Research calendar and promoting it in your section’s Slack channels can be great ways of soliciting note-takers. 

