---
layout: handbook-page-toc
title: "UX research at GitLab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The goal of UX research at GitLab is to connect with GitLab users all around the world and gather insight into their behaviors, motivations, and goals. We use these insights to inform and strengthen product and design decisions.

UX Researchers are one of the many GitLab Team Members who conduct user research. Other roles, such as Product Managers and Product Designers, frequently conduct research with guidance from the UX Research team. Product Designers and Product Managers should complete the [Research Shadowing](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/research-shadowing/) process during their onboarding to gain an understanding of how research is conducted at GitLab.

Looking for resources to help you in your UX research? Check them out here (link coming soon).
Do you have questions about UX Research? The UX Research team is here for you! Reach out in the #ux_research Slack channel.
 
## Team structure
Each UX Researcher is assigned to multiple, related stage groups, so they can focus on a larger product area. They work closely with Product Managers and Product Designers to ensure research projects are focused and provide answers to design questions. You can find more information on these stage groups [here](https://about.gitlab.com/handbook/product/product-categories/#devops-stages).

The [Research Coordinator](https://about.gitlab.com/handbook/engineering/ux/ux-research-coordination/) at GitLab is responsible for managing all aspects of participant recruitment for GitLab’s user experience research studies, including  (but not limited to) sourcing, outreach, screening, scheduling, participation agreements, and incentives management.
 
## UX research workflow
The UX Research department works within the [Product Development Flow](/handbook/product-development-flow/#overview--philosophy) as they partner with Product Management and Product Design.  Additional details can be found here (link coming soon) on how the GitLab UX Research Department operates. 

UX research can be divided into two categories:
* **Problem Validation** - seeks to provide decision makers with a well understood, clearly articulated customer problem
* **Solution Validation** - critically assesses if the product/feature/design has indeed solved the problem that was initially intended to be solved. 
 
## Steps in the UX research process:
1. [Defining goals, objectives, and hypotheses](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/defining-goals-objectives-and-hypotheses/)
2. [Recruiting Participants](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/recruiting-participants/)
3. [Collecting Useful Data](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/collecting-useful-data/)
4. [Analyzing and Sythenzing Data](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/analyzing-research-data/)
5. [Documenting Research Findings](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/documenting-research-findings/)
