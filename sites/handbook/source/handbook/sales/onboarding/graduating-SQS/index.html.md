---
layout: handbook-page-toc
title: "Graduating from Sales Onboarding"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Graduating from Sales Onboarding
In order to officially “graduate” from Sales Onboarding at GitLab, we have identified a few components that are considered milestones of achievement (many of these must be done concurrently to ensure completion):

Sales Roles:
*  Complete GitLab General Onboarding issue
*  Complete Sales Quick Start learning path in Google Classroom prior to the SQS Workshop
*  Complete the Sales Quick Start Workshop
*  Deliver GitLab Value Pitch
*  Articulate the Command of the Message Mantra
*  Complete a series of discovery calls:
   - Submit 1 recorded mock discovery call in SQS pre-work
   - Complete 1 mock discovery call at the SQS Workshop
   - Submit 1 “Live Lead” after the SQS Workshop within 30 days
*  Review and obtain approval from your manager for a territory and account plan
*  Close first deal within the first 90 days of starting at GitLab

Customer Success Roles: 
*  Complete GitLab General Onboarding issue
*  Complete Sales Quick Start learning path in Google Classroom prior to the SQS Workshop
*  Complete the Sales Quick Start Workshop
*  Deliver the role based Capstone 
   * TAM: Mock customer kickoff
   * SA: Build and deliver mock demo
   * PSE: Statement of Work
*  Articulate the Command of the Message Mantra
*  Kickoff first customer engagement within the first 90 days of starting at GitLab
