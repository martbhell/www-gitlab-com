---
title: "GitLab's Functional Group Updates: November 20th - December 8th" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: company
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working from on November 20th - December 8th"
canonical_path: "/blog/2017/12/08/functional-group-updates/"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Friday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/group-conversations/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Team

[Presentation slides](https://docs.google.com/presentation/d/1Km3GHdjOEAt1rnAS4Er6oMvCkah0fWU5eQh1ETMj0ZU/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/yqqmAtBZpaA" frameborder="0" allowfullscreen="true"></iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### CI/CD Team

[Presentation slides](https://docs.google.com/presentation/d/1MaI5MhnebvSHvJpXnFhcR2WPdeIxC4nwGKX-q6R9vWk)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/sqrZl64nyFM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Build Team

[Presentation slides](https://docs.google.com/a/gitlab.com/presentation/d/1CqiScyCt_H_jDNfRdhsej1bamk9aQ-RlVHa9bIq4IsE/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/sZl9ipor10c" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Frontend Team

[Presentation slides](https://docs.google.com/presentation/d/1WxpSjbh_tCT59jff2_lG7ww-6RCf6qldgyfO5E4-g_Y/edit#slide=id.g153a2ed090_0_63)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/_BUc_AQOITM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Prometheus Team

[Presentation slides](https://docs.google.com/presentation/d/1sRAfWQngk2yVI4pTdkfgOw7GO_vH406Ap-2S6--B7-o/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/QxzEym01RAs" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Edge Team

[Presentation slides](http://gitlab-org.gitlab.io/group-conversations/edge/2017-12-05/#1)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/rGJsTl5r_cU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Research Team

[Presentation slides](https://docs.google.com/presentation/d/1L2auwPO2V2Q4GZik1YKIQh8pPn4DJPvRAMux7iYwALs/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Tsmx_8skJY8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
