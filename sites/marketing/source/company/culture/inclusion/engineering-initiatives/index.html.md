---
layout: markdown_page
title: "Diversity, Inclusion & Belonging Engineering Initiatives"
description: "Learn more about GitLab Diversity, Inclusion & Belonging Engineering Initiatives."
canonical_path: "/company/culture/inclusion/engineering-initiatives/"
---

This page is in [draft](/handbook/values/#everything-is-in-draft). We are working to build out this page and gather all the running initiatives/partnerships in Engineering. 

At GitLab, Diversity, Inclusion & Belonging is infused into our company culture, from our [values](/handbook/values/) to our [all-remote way of working](/company/culture/all-remote/). 
Engineering partners closely with our [Diversity, Inclusion & Belonging](/company/culture/inclusion/) team to ensure we’re building a diverse and inclusive workforce around the globe as the company continues to grow. This page provides an overview of DIB Engineering Initiatives:


* [Upstream Diversity Working Group](/company/team/structure/working-groups/upstream-diversity/)
* [Minorities in Tech Mentoring program](/company/culture/inclusion/erg-minorities-in-tech/mentoring/)
* [FY21-Q3 KR: 75% of Managers in the Engineering Division complete DIB training](75% of Managers in the Engineering Division complete DIB training => 72.45%)
* [Team Member Resource Groups](/company/culture/inclusion/erg-guide/#how-to-join-current-tmrgs-and-their-slack-channels)
* [Diversity, Inclusion and Belonging GitLab initiatives](/company/culture/inclusion/#diversity-inclusion--belonging-mission-at-gitlab)
* [Engineering Internship Pilot Program](/handbook/engineering/internships/)
* [Recruiting: Diversity lifecycle metrics](/handbook/hiring/metrics/#diversity-lifecycle-applications-recruited-interviewed-offers-extended-offers-accepted-and-retention)
* In House Diversity, Inclusion and Belonging Training Certification - More to come 
