---
layout: markdown_page
title: "Webcasts - Customer Success"
---

## On this page
{:.no_toc}

- TOC
{:toc}  


## Upcoming Customer Success Webcasts  

Interactive HA/Geo Q&A with GitLab's Infrastructure Team  
<strong>June 18th, 9-10am PDT / 12-1p EDT</strong>   
<a href="/webcast/customer-success/ha-geo-setup">Register</a> to join our infrastructure team in a fireside chat on how we've configured gitlab.com's HA/Geo set up.
