---
layout: markdown_page
title: "Category Direction - GitLab Self-Monitoring"
description: "GitLab provide administrators with a great out-of-the-box observability solution for monitoring their GitLab instance. Learn more!"
canonical_path: "/direction/monitor/self-monitoring/"
---

- TOC
{:toc}

## Introduction and how you can help
Thank you for visiting this category strategy page on GitLab self monitoring project. The Self monitoring project is open for community contribution (including non-Development GitLab team members). GitLab instrumentation is currently not in the scope of this project. If you have any questions or suggestions, please reach out to [Kevin Chu](https://gitlab.com/kbychu), group product manager via [Zoom](https://calend.ly/kchu-gitlab) or [Email](mailto:kchu@gitlab.com)

## Target Audience and Experience
The target audience is primarily ~"Persona: Systems Administrator," in particular the administrators responsible for operating GitLab.

We also plan to allow administrators to expose a user facing status page, to easily communicate the health of the instance and any active incidents.

## Using GitLab to monitor GitLab

The Self-monitoring project MVC officially [shipped](https://about.gitlab.com/releases/2020/02/22/gitlab-12-8-released/#gitlab-self-monitoring-project) in 12.8. With it GitLab administrators can now gain insights into the health of their GitLab instances. Once [activated](https://docs.gitlab.com/ee/administration/monitoring/gitlab_self_monitoring_project/), a default dashboard with set of KPI's will be available for the project out of the box, users will be able to [add metrics](https://docs.gitlab.com/ee/administration/monitoring/gitlab_self_monitoring_project/#adding-custom-metrics-to-the-self-monitoring-project) to the dashboard from the list of [availabe metrics](https://docs.gitlab.com/ee/administration/monitoring/gitlab_self_monitoring_project/#adding-custom-metrics-to-the-self-monitoring-project) which automatically picked up by our internal Prometheus instance.


#### What's Next & Why

The self-monitoring projects now act as a standard GitLab project, its default dashboard is a custom dashboard, the metrics streamed to GitLab's metrics chart in a similar way to a project with [manual prometheus](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#manual-configuration-of-prometheus) configuration. Improvements to the self monitoring project will be mainstreamed in a similar fashion to any other improvment the [APM team](https://about.gitlab.com/handbook/engineering/development/ops/monitor/apm/#adding-new-issues) is working on, this way we can garantee that any new features or functionality that gets add to our product will be available immediately for the GitLab self monitoring project.

## Maturity Plan
* [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/783)


## Top related issues (Handled by the APM team)
* [Support alerts on custom defined metrics/dashboards](https://gitlab.com/gitlab-org/gitlab/issues/194267)
* [Exclude health endpoints from HTTP request metrics](https://gitlab.com/gitlab-org/gitlab/issues/199434)
* [Kubernetes cluster blocks project manual Prometheus configuration](https://gitlab.com/gitlab-org/gitlab/-/issues/207233)
* [Add instructions on monitoring GDK with Prometheus](https://gitlab.com/gitlab-org/gitlab-development-kit/-/merge_requests/1085)

