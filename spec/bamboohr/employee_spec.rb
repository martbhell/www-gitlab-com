describe BambooHR::Employee do
  subject(:employee) { described_class.new(data, manager_id) }
  let(:data) do
    {
      'employeeNumber' => 1,
      'country' => 'US',
      'department' => 'Design',
      'preferredName' => 'John',
      'lastName' => 'Smith',
      'jobTitle' => 'Designer',
      'hireDate' => '2015-01-01'
    }
  end
  let(:manager_id) { 100 }

  describe '#bamboohr_id' do
    subject { employee.bamboohr_id }

    it { is_expected.to eq(data['employeeNumber']) }
  end

  describe '#country' do
    subject { employee.country }

    it { is_expected.to eq(data['country']) }
  end

  describe '#department' do
    subject { employee.department }

    it { is_expected.to eq(data['department']) }
  end

  describe '#manager_id' do
    subject { employee.manager_id }

    it { is_expected.to eq(manager_id) }
  end

  describe '#name' do
    subject { employee.name }

    it { is_expected.to eq('John Smith') }

    context 'when preferred name is missing' do
      let(:data) { super().merge('preferredName' => nil, 'firstName' => 'Alex') }

      it { is_expected.to eq('Alex Smith') }
    end
  end

  describe '#role' do
    subject { employee.role }

    it { is_expected.to eq(data['jobTitle']) }
  end

  describe '#start_date' do
    subject { employee.start_date }

    it { is_expected.to eq(data['hireDate']) }
  end

  describe '#similarity' do
    subject { employee.similarity(team_entry) }

    context 'when exact match' do
      let(:team_entry) { { 'name' => 'John Smith', 'role' => 'Designer', 'start_date' => '2015-01-01' } }

      it { is_expected.to eq(1.5) }
    end

    context 'when it does not match' do
      let(:team_entry) { { 'name' => 'Bobby White', 'role' => 'Support Engineer', 'start_date' => '2020-01-01' } }

      it { is_expected.to eq(0) }
    end

    context 'when it partially matches' do
      let(:team_entry) { { 'name' => 'Jonathan Smith', 'role' => 'Designer', 'start_date' => '2015-01-01' } }

      it { is_expected.to eq(1.1) }
    end
  end
end
